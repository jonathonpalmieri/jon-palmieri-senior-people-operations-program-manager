## Letter of Intent

Hello anechan, 

I am reaching out with great enthusiasm regarding the People Operations Program Manager within GitLab, I was immediately drawn to the prospect of contributing to a company renowned for its operational excellence and innovative practices. My career has been dedicated to harnessing the power of people and analytics to enhance operations and empower employees, a philosophy that aligns seamlessly with the core responsibilities and ethos of GitLab.

My professional journey in people operations and technology has been marked by significant contributions to process optimization and employee empowerment. As a Lead Talent Acquisition Consultant, I pioneered the automation of Oracle BI reporting and subsequently led the implementation of an integrated CRM system. 

As an avid learner, I have always sought to demystify technology, simplifying intricate concepts to facilitate informed stakeholder decision-making throughout the project management process. I pride myself on the ability to analyze people and processes to design innovative and effective operations, workflows, and technologies to maximize employee experience and performance. I have a proven track record of developing and managing strategies that facilitate clear and purposeful information exchange aligned with strategic goals.

I would love an opportunity to talk further with you about this opportunity. I am including my resume below for your review.

Best regards,

Jonathon Palmieri  


## Resume | Contact information removed for privacy

### **Technology & People Operations**

As a strategic HRM professional, I bring a comprehensive skill set in providing analytics and interpreting results to inform key business decisions. My expertise includes managing the complete cycle of workforce planning, from initial data collection to tracking and governance, ensuring alignment with business strategies to identify workforce implications and bridge role/skill gaps while supporting talent strategies. My leadership has driven competency shifts and cultural changes towards data-driven decision-making, enhancing workforce productivity, performance, and utilization. Renowned for my subject matter expertise is further enriched by a solid background in statistical analysis, research, and organizational assessment, cultivated from advanced education and extensive practical experience.

#### **Human Resources**

| **Resource Management & Employee Relations** |
| --- |
| Employee engagement, satisfaction, retention, career planning, role & skill gap assessment, resource utilization, training & development, labor market research & analysis, leadership & employee assessments & surveying, diversity, equity, & inclusion, labor laws & compliance, conflict resolution |

| **Technology Systems (ATS/CRM/HRIS)** |
| --- |
| Greenhouse, Taleo, JazzHR, Bullhorn, ICIMS, ZOHO, Workday, ADP, HireVue, PeopleSoft, SAP, Salesforce, HubSpot, SugarCRM, ZOHO, SAP, SuiteCRM, Marketo |

| **Data Visualization & Analysis** |
| --- |
| Talent Analytics (TalentNeuron, LinkedIn Insights), FMV & compensation & benefit analysis, retention analysis, performance management analysis, workforce planning, job posting analysis, survey analysis |

| **Additional Knowledge Areas** |
| --- |
| Digital marketing, sales, resource surveying, growth & workforce planning, personality assessments, project management, onboarding & offboarding, immigration, mental wellness, employee motivation, mindfulness & meditation, consulting. |

#### **Technical**
| **Programming** |
| --- |
| Java, Python, JavaScript, HTML, CSS, RegEx, Spring Boot, Lamp, SSL |

| **Data Technologies** |
| --- |
| MySQL, PostgreSQL, ETL/ELT, Data Pipelining, WebDav, CalDav, Apache, S3 Storage, SolR, Elasticsearch, Power BI, Excel, Chart.js, SPSS, PSPP, OpenRefine, Oracle BI, Google Analytics |

| **Automation & AI** |
| --- |
| Power Automate, N8N, IFTT API, Jsoup/Beautiful Soup, OpenAI/GPT-4, Torch, Llama, NLP, Computer Vision/OpenCV, ChromaDB, AI techniques (Random Forest, N-Gram, Bag of Words, Sentiment Analysis, NLP, etc.) |

| **Open-Source Technologies** |
| --- |
| Shlink, Nextcloud, OpenVPN, Certbot, Collabora Office, Jitsi, WordPress, PrivateGPT, OpenGPT Android, Ubuntu/Linux, Kali Linux |

| **APIs & Web** |
| --- |
| Webhooks, HTTP, RESTful & SOAP APIs |



### **PROFESSIONAL EXPERIENCE**

#### Rex Recruiting | Talent & HCM Consultant | 2019 to Present

Rex Recruiting excels in providing search services tailored to discover top-tier candidates while also offering strategic human capital analytics and technology consulting. With a strong foundation in data-driven decision-making, we provide road maps toward advanced technology solutions that enable optimized business processes, improve employee experience, and maximize ROI.

- Deployed data-driven real-time dashboarding and reporting that gives executive leadership critical insights into human capital metrics such as cost-per-hire, workforce planning, retention, performance, market/competitor compensation and benefits trends, and marketing ROI, bolstering strategic decision-making processes.
- Implemented strategic full-cycle hire-to-retire HCM business process digitization and transformation initiatives, including future-state road mapping, system selection, data strategy, support, and training for various small to mid-sized companies.
- Developed and maintained positive client executive relationships including new business development, RFPs, negotiations, contracting, and constant communications.
- Recognized as a subject matter expert adept in consulting on various HR topics, including operational efficiencies, compensation and benefits, recruitment, retention, engagement, SEO, and significantly reducing cost-per-hire and time-to-fill for top-tier clients such as Moody’s, GE, Deloitte, Nationwide, and more.
- Spearheaded the deployment of advanced HR technology solutions, including comprehensive systems integrations and automation, leading to improved business processes that maximize employee and candidate experience and ROI.
- Led the development of standard work for Global Talent Acquisition operations utilizing Microsoft tools (Power Automate, Excel, Power Bi), automating aspects of the candidate screening and interview process, reducing the time to fill, cost per hire, and optimizing open requisitions and headcount forecasting.
- Designed, customized, and developed technology systems, including website, CRM/ATS, payroll, cloud storage, and implemented various integrations, automation, and AI technologies.
- Embedded in various organizations managing a full-cycle recruitment desk for hard-to-fill, highly technical, and executive-level strategic recruitment efforts and trained and managed teams of talent acquisition professionals.

#### Hartford HealthCare | Senior Talent Acquisition Consultant & Team Lead | 2016 to 2019 (FTE), 2020-2021 (Contract Consultant)

Hired into newly formed centralized Provider Recruitment Team to capitalize hiring process for Advanced Practitioners and Physicians. Advocated operational efficiency and quality candidate experience throughout recruitment by collaborating with onboarding, credentialing, human resources, operations, and business units. Implemented various technology optimizations and adoptions to improve business operations and effectiveness. Highlighted value:

- Developed several Oracle BI and MySQL reports that were automatically distributed in Excel, reducing manual reporting requirements and easing analysis.
- Analyzed Oracle BI Taleo reports to identify a 73% candidate application abandonment rate, assessed the application workflow for improvements, and developed an RFP to implement integrated CRM and strategic systems integrations and automation.
- Formed and led the team to implement an integrated CRM system, reducing candidate abandonment to 18% and drastically improving operations and technology workflows for onboarding, immigration, payroll, and credentialing.
- Slashed time-to-fill over 36% by sourcing, screening, qualifying, and coordinating the hiring of 200+ practitioners annually.
- Boosted applicants 39%, training Talent Acquisition team on digital inbound and outbound marketing best practices.
- Utilizing SQL, Oracle BI, Excel, and JavaScript created and presented ROI reports and dashboards to track marketing effectiveness against job board expenses. Visualized data encompassing job posting views, applications, and initial interview counts.
- Improved workforce planning by generating a labor location analysis geocoded heatmap based on licensure and internal data of Advanced Practitioners in CT, RI, MA, NY. Educated executive team on statistics.
- Hired, trained, and led two Recruiters within the Advanced Practice Provider Recruitment team with an average 200 requisition load, and 71-day time to fill (42% reduction from formation).


#### Hobson Associates | Executive Recruiter & Team Lead | 2014-2016

Performedfull cycle recruiting of top talent, including sourcing, prospecting, qualifying, negotiating employment terms, and on-boarding. Recruited general business and industry-specific candidates in technology, data analytics, and financial industries. _Highlighted value:_

- Propelled past quota by $350K, increasing Insurance Team's annual billings 50%.

- Secured Fortune-level and startup clients by networking and collaborating with industry executives.
- Instituted the Technology, Financial Services, and Insurance Team, increasing client billings $750K within first year by training and empowering new hires on recruiting best practices.
- Awarded Rookie of the Year for doubling database of recruits, accumulating organic responses to open roles through innovative networking and social media campaigns.

### **Additional Experience:**

- _Human Resource Generalist & Recruiter | Integrated Physicians Management Services | 2013 to 2014_
- _Human Resource and Education Generalist Internship | A.W. Hastings | 2013_

### **EDUCATION**

**Master of Science in Industrial Organizational Psychology** | University of Hartford | West Hartford, CT (3.95 GPA)

**Circulum Including:** Performance Evaluation & Management, Research Methods, Research Statistics, Psychology of Teams, Motivation in the Workplace, Consulting, Personnel Psychology, Managing Organizational Process, Global Talent Management, Stress & Stress Management_

**Bachelor of Arts in Psychology, Minor in Marketing** | Central Connecticut State University | New Britain, CT

**Fellow |** American Association of Physician & Provider Recruitment (formerly ASPR)

### **COMMUNITY LEADERSHIP**

**Awarded Social Engineering Competitor | Def Con**
Our team was selected out of hundreds of applicants to compete in a live OSINT collection and voice phishing "Vishing" competition, where we came in 11th place. We were responsible for obtaining various open-source intelligence targets through the web as well as cold-calling employees of a specified company.

**Recruiting Community Head Moderator | Reddit**
Manage a community of 65k+ members of recruiters, hiring managers, and vendors in addition to helping moderate the r/AskHR and r/HumanResources subreddits (150k+ members).